package com.company;

import java.util.HashSet;

public class Employee {
    public int employeeID;
    private HashSet<String> qualifications = new HashSet<String>();

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public Employee(int employeeID) {
        this.employeeID = employeeID;
    }

    public void addQualification(String qualification)
    {
        qualifications.add(qualification);
    }

    public void showQualification()
    {
        for(String qualification : qualifications)
            System.out.println(qualification);
    }
}
