package com.company;

public class Main {

    public static int count = 0;
    public static int mutex = 1;
    public static void main(String[] args) throws InterruptedException {
	// write your code here
        Thread player1 = new Thread(new Tick());
        Thread player2 = new Thread(new Tock());
        player1.start();
        player2.start();
        player1.join();
        player2.join();
        System.out.println("Game Ended");

        //Try with wait() and notify()
    }
}
