package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String inputFile = consoleReader.readLine();
        String outputFile = consoleReader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputFile));
        String line;
        fileWriter.write("Results");
        fileWriter.write("\n");
        while((line = fileReader.readLine()) != null)
        {
            String[] data = line.split(",");
            int sum = summer(data);
            System.out.println(sum);
            fileWriter.write(((Integer)sum).toString());
            fileWriter.write("\n");
        }
        consoleReader.close();
        fileReader.close();
        fileWriter.close();
    }

    private static int summer(String[] data) {

        int sum = 0;
        for(String str : data)
        {
            int arg = Integer.parseInt(str);
            sum+=arg;
        }

        return sum;
    }
}
