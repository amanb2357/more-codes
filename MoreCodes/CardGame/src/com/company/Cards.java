package com.company;

public class Cards {
    public static int[] cards = new int[52];

    public Cards() {
        for(int i=0;i<52;i++)
            cards[i] = i+1;
    }

    public void choosePlayer(Player1 p1, Player2 p2, Player3 p3) {
        System.out.println("p1: " + p1.cardChosen);
        System.out.println("p2: " + p2.cardChosen);
        System.out.println("p3: " + p3.cardChosen);
        if(p2.cardChosen>p1.cardChosen&&p2.cardChosen>p3.cardChosen)
            System.out.println("p2 wins");
        if(p1.cardChosen>p2.cardChosen&&p1.cardChosen>p3.cardChosen)
            System.out.println("p2 wins");
        if(p3.cardChosen>p1.cardChosen&&p3.cardChosen>p2.cardChosen)
            System.out.println("p2 wins");
    }
}
