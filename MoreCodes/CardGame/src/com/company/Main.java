package com.company;

public class Main {

    public static void main(String[] args) throws InterruptedException {
	// write your code here
    Cards deck = new Cards();
    Player1 p1 = new Player1();
    Player2 p2 = new Player2();
    Player3 p3 = new Player3();
    p1.start();
    p2.start();
    p3.start();
    p1.join();
    p2.join();
    p3.join();
    deck.choosePlayer(p1, p2, p3);
    }
}
