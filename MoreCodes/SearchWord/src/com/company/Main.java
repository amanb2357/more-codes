package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String inputFile = consoleReader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));

        System.out.println("Choices\n1. Search with word\n2. Replace with word");
        int choice = Integer.parseInt(consoleReader.readLine());

        switch (choice){
            case 1:
            {
                String word = consoleReader.readLine();
                String line;
                while ((line = fileReader.readLine()) != null)
                {
                    if(line.contains(word))
                    {
                        System.out.println(line);
                    }
                }
                break;
            }
        }

    }
}