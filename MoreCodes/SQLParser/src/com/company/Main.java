package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader fileReader = new BufferedReader(new FileReader("C:\\Users\\amabhard\\Desktop\\people.txt"));
        //mapping
        HashMap<String, Integer> columnMap = columns(fileReader);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String query = reader.readLine();
        char[] letters = query.toCharArray();
        int fromBeginning = 0;
        for(int i=0;i<query.length()-4;i++)
        {
            if(letters[i] == 'f'&&letters[i+1] == 'r'&&letters[i+2] == 'o'&&letters[i+3] == 'm')
            {
                fromBeginning = i;
                break;
            }
        }
        String fieldsString = query.substring(7,fromBeginning-1);

        String[] fieldArray = fieldsString.split(",");
        List<Integer> indexesToPrint = indexFinder(fieldArray, columnMap);
        //System.out.println(indexesToPrint.toString());
        fileReader.close();
        BufferedReader fileReader2 = new BufferedReader(new FileReader("C:\\Users\\amabhard\\Desktop\\people.txt"));
        String line;
        fileReader2.readLine();
        while((line = fileReader2.readLine())!=null)
        {
            //System.out.println(line);
            String[] data = line.split(",");
            //System.out.println(data[0]);
            for(Integer num : indexesToPrint)
                System.out.print(data[(int)num] + " ");
            System.out.println();
        }
    }

    private static HashMap columns(BufferedReader fileReader) throws IOException {
        HashMap<String, Integer> columns = new HashMap<String, Integer>();
        String fields = fileReader.readLine();
        //System.out.println(fields);
        String[] Indexes = fields.split(" ");
        for(int i=0;i<Indexes.length;i++){
            columns.put(Indexes[i],i);
        }
        return columns;
    }

    private static ArrayList indexFinder(String[] fieldArray, HashMap columnMap) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(String element : fieldArray)
        {
            list.add((Integer) columnMap.get(element));
            //System.out.println((Integer) columnMap.get(element));
        }
        return list;
    }
}
